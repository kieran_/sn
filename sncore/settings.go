package sncore

import (
	"encoding/json"
	"log"
	"os"
)

type Settings struct {
	BindAddress string
	MySQLServer string
	UseSSL      bool
	SSLCrt      string
	SSLKey      string

	WorkerThreads          int
	MaxClients             int32
	ClientWriteQueueLength int32
	ClientBufferSize       int64
	BroadcastQueueLength   int64
	CheckOrigin            bool
	Spamtime               int64

	DHTRouters   string
	DHTCheckTime int32
	DHTPort      int32
	InfoHash     string

	BindNodeAddress     string
	NodePort            int
	MaxNodes            int32
	NodeWriteBufferSize int64
	NodeExchange        bool
	NodeStatInterval    int32
}

func (s *Settings) Load() {
	//Set default values
	s.BindAddress = ":8080"
	s.BindNodeAddress = ":600"
	s.MySQLServer = "root:root@/localhost"
	s.DHTRouters = "de.magnets.im:6881,router.bittorrent.com:6881"
	s.InfoHash = ""
	s.ClientBufferSize = 1024
	s.ClientWriteQueueLength = 256
	s.BroadcastQueueLength = 1024
	s.CheckOrigin = false
	s.Spamtime = 100
	s.DHTCheckTime = 30
	s.DHTPort = 50000
	s.NodePort = 600
	s.MaxNodes = 200
	s.NodeWriteBufferSize = 1024
	s.NodeExchange = true
	s.MaxClients = 1024
	s.WorkerThreads = 2
	s.UseSSL = false
	s.NodeStatInterval = 10

	fileInfo, fierr := os.Stat("settings.json")
	file, err := os.Open("settings.json")

	if err == nil && fierr == nil {
		defer func() {
			file.Close()
		}()

		data := make([]byte, fileInfo.Size())
		_, fer := file.Read(data)

		if fer == nil {
			er := json.Unmarshal(data, s)
			if er != nil {
				log.Fatalln("Parse Error: ", er)
			}
		} else {
			log.Fatalln("Read Error: ", fer)
		}
	} else {
		if os.IsNotExist(err) {
			s.Save()
			log.Println("Saving blank config..")
		} else {
			log.Fatalln("Error: ", err)
		}
	}
}
func (s *Settings) Save() {
	file, err := os.Create("settings.json")

	if err == nil {
		defer file.Close()
		data, err := json.Marshal(s)
		if err == nil {
			file.Write(data)
		} else {
			log.Fatalln("Error: ", err)
		}
	}
}
