package sncore

import (
	"log"
	"net"
	"time"
)

type RemoteNodeStats struct {
	clients int
}

type RemoteNode struct {
	writeQueue chan []byte
	sock       net.Conn
}

func (rn *RemoteNode) Read() {
	defer func() {
		rn.sock.Close()
		close(rn.writeQueue)
	}()

	buf := make([]byte, 0, 1024)
	for {
		_, err := rn.sock.Read(buf)
		if err == nil {
			msg := string(buf)
			log.Println("Got message from node:", msg)
			switch msg {
			case "PING":
				{
					rn.Send("PONG")
					break
				}
			case "STATS":
				{
					break
				}
			}
		} else {
			break
		}
	}
}
func (rn *RemoteNode) Write() {
	ticker := time.NewTicker(time.Duration(S.NodeStatInterval) * time.Second)
	for {
		select {
		case msg, ok := <-rn.writeQueue:
			{
				if ok {
					rn.sock.Write(msg)
				} else {
					break
				}
			}
		case <-ticker.C:
			{
				rn.Send("SOME STATS YO")
			}
		}
	}
}

func (rn *RemoteNode) Send(msg string) {
	rn.writeQueue <- []byte(msg)
}
