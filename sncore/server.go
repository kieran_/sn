package sncore

import (
	"github.com/gorilla/websocket"
	"log"
	"net/http"
)

var (
	upgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}
)

type Server struct {
	listenip       string
	port           uint
	name           string
	clients        map[*Client]bool
	BroadcastQueue chan *Packet
}

func (s *Server) Setup() {
	if !S.CheckOrigin {
		upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	}
	s.clients = make(map[*Client]bool)
	s.BroadcastQueue = make(chan *Packet, S.BroadcastQueueLength)

	go s.Broadcast()
}

func (s *Server) Broadcast() {
	for {
		m, ok := <-s.BroadcastQueue
		if ok {
			for c := range s.clients {
				select {
				case c.WriteQueue <- m:
					{
						log.Println("Writing to client", c)
					}
				}
			}
		}

	}
}

func (s *Server) NewClient(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", 405)
		return
	}
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	c := &Client{ws: ws, server: s, room: "default", WriteQueue: make(chan *Packet, S.ClientWriteQueueLength)}
	s.clients[c] = true
	log.Println(ws.RemoteAddr(), "connected")
	go c.Read()
	go c.Write()
}
