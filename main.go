package main

import (
	"flag"
	"log"
	"net/http"
	"sn/sncore"
)

func main() {
	S := new(sncore.Settings)
	S.Load()
	N := new(sncore.Node)
	N.Setup()
	SERV := new(sncore.Server)
	SERV.Setup()

	var addr = flag.String("addr", S.BindAddress, "http service address")
	flag.Parse()

	http.HandleFunc("/", SERV.NewClient)

	if S.UseSSL {
		log.Println("Server starting (SSL)")
		err := http.ListenAndServeTLS(*addr, S.SSLCrt, S.SSLKey, nil)
		if err != nil {
			log.Fatalln("ListenAndServeSSL: ", err)
		}
	} else {
		log.Println("Server starting")
		err := http.ListenAndServe(*addr, nil)
		if err != nil {
			log.Fatalln("ListenAndServe: ", err)
		}
	}

}
