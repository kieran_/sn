package sncore

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/websocket"
	"log"
	"strings"
	"time"
)

type Client struct {
	ws         *websocket.Conn
	room       string
	session    string
	WriteQueue chan *Packet
	server     *Server
}

func (c *Client) Read() {
	defer func() {
		c.ws.Close()
		close(c.WriteQueue)
	}()
	lastMsg := time.Now().UnixNano()

	for {
		rsp := new(Packet)
		err := c.ws.ReadJSON(rsp)
		if err == nil {
			now := time.Now().UnixNano()
			if now-lastMsg >= S.Spamtime*1000000 {
				lastMsg = now

				if rsp.Id == SN_AUTH {
					//Check auth message
					str, ok := rsp.Message.(string)

					if ok {
						c.session = str
						if !c.CheckAuth() {
							rsp.Id = SN_ERROR
							rsp.Message = "Auth failed"
							c.SendMsg(rsp)
						} else {
							rsp.Id = SN_AUTH
							rsp.Message = "Auth ok!"
							c.SendMsg(rsp)
						}
					} else {
						rsp.Id = SN_ERROR
						rsp.Message = "Auth failed, input was in the wrong format"
						c.SendMsg(rsp)
					}
				} else if c.CheckAuth() {
					switch rsp.Id {
					case SN_CHAT:
						{
							if c.CheckRoom() {
								c.server.BroadcastQueue <- rsp
							} else {
								rsp.Id = SN_ERROR
								rsp.Message = "You are not in a room!"
								c.SendMsg(rsp)
							}
							break
						}
					default:
						{
							rsp.Id = SN_ERROR
							rsp.Message = "Unknown packet id"
							c.SendMsg(rsp)
						}
					}
				} else {
					log.Println("User is not authorized to send messages, closing connection to", c.ws.RemoteAddr())
					break
				}
			} else {
				log.Println("Spam deteced, closing connection to", c.ws.RemoteAddr())
				break
			}
		} else {
			log.Println(c.ws.RemoteAddr(), "disconnected (", err, ")")
			break
		}
	}
}

func (c *Client) Write() {
	for {
		msg, ok := <-c.WriteQueue
		if ok {
			c.ws.WriteJSON(msg)
		} else {
			break
		}
	}
}

func (c *Client) SendMsg(p *Packet) {
	c.WriteQueue <- p
}

func (c *Client) CheckAuth() bool {
	db, err := sql.Open("mysql", S.MySQLServer)

	if err != nil {
		log.Println("Mysql connect failed,", err.Error())
	} else {
		defer db.Close()

		smt, err := db.Prepare("select * from session where token = ?")
		if err != nil {
			log.Println("Prepared statment create failed,", err.Error())
		} else {
			defer smt.Close()

			var (
				id     int
				token  string
				ip     string
				expire time.Time
			)
			err = smt.QueryRow(c.session).Scan(&id, &token, &ip, &expire)
			if err == nil {
				rem := strings.Split(c.ws.RemoteAddr().String(), ":")[0]
				if ip == rem {
					return true
				} else {
					log.Println("IP did not match session,", ip, "==", rem)
				}
			} else {
				log.Println("Auth failed,", err.Error())
			}
		}
	}
	return false
}

func (c *Client) CheckRoom() bool {
	return false
}
