package sncore

import (
	"github.com/nictuku/dht"
	"log"
	"net"
	"strconv"
	"strings"
	"time"
)

type Node struct {
	server    *Server
	nodes     map[*net.Conn]RemoteNode
	writeChan chan []byte
}

func (n *Node) CheckNode() {
	for _, node := range n.nodes {
		node.Send("STATS")
	}
}

func (n *Node) Listen() {
	conn, err := net.Listen("tcp", S.BindNodeAddress)
	if err == nil {
		for {
			nc, er := conn.Accept()
			if er == nil {
				log.Println("New node connected:", nc.RemoteAddr().String())
				rn := RemoteNode{sock: nc, writeQueue: make(chan []byte, S.NodeWriteBufferSize)}
				n.nodes[&nc] = rn

				go rn.Read()
				go rn.Write()
			}
		}
	}

}

func (n *Node) Setup() {
	n.nodes = make(map[*net.Conn]RemoteNode, S.MaxNodes)

	go n.Listen()

	ih, err := dht.DecodeInfoHash(S.InfoHash)
	if err == nil {
		cfg := dht.NewConfig()
		cfg.DHTRouters = S.DHTRouters
		cfg.Port = int(S.DHTPort)

		d, errd := dht.New(cfg)
		if errd == nil {
			go d.Run()

			go func() {
				//Manage socket connections to the nodes
				for r := range d.PeersRequestResults {
					for _, peers := range r {
						for _, x := range peers {
							ip := strings.Split(dht.DecodePeerAddress(x), ":")

							//Check is already connected to peer
							makeConn := true
							for _, v := range n.nodes {
								if strings.Split(v.sock.RemoteAddr().String(), ":")[0] == ip[0] {
									makeConn = false
									break
								}
							}
							if makeConn {
								ip[1] = strconv.Itoa(S.NodePort)
								ipj := strings.Join(ip, ":")

								//log.Println("Trying node connection:", ipj)
								conn, err := net.Dial("tcp", ipj)
								if err == nil {
									log.Println("New node connected:", conn.RemoteAddr().String())
									rn := RemoteNode{sock: conn, writeQueue: make(chan []byte, S.NodeWriteBufferSize)}
									n.nodes[&conn] = rn

									go rn.Read()
									go rn.Write()
								}
							}
						}
					}
				}
			}()

			go func() {
				log.Println("DHT Running for info hash:", S.InfoHash)
				for {
					d.PeersRequest(string(ih), true)
					time.Sleep(time.Duration(S.DHTCheckTime) * time.Second)
				}
			}()

		} else {
			log.Println("DHT failed to init,", errd.Error())
		}
	} else {
		log.Println("Hash is invalid", S.InfoHash, err.Error())
	}
}
