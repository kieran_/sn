package sncore

import (
	"log"
)

type PacketCode int

const (
	SN_VERSION int = 1
)

const (
	SN_UNKNOWN PacketCode = iota
	SN_ERROR
	SN_AUTH
	SN_JOIN_ROOM
	SN_CHAT
	SN_LEAVE_ROOM
)

type Packet struct {
	Id      PacketCode
	Message interface{}
}

func (p *Packet) Handle() (*Packet, *Packet) {
	var reply *Packet = nil
	var broadcast *Packet = nil

	switch p.Id {
	default:
		{
			log.Println("Unknown packet code")
		}
	}

	return reply, broadcast
}
