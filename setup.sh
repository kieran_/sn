#!/bin/bash

apt-get install mercurial git subversion golang -y

GPATH=`pwd`
export GOPATH="$GPATH"
echo "GOPATH SET TO $GPATH"
mkdir "$GPATH/src"
cd "$GPATH/src"

git clone https://kieran_@bitbucket.org/kieran_/sn.git

go get github.com/gorilla/websocket
go get github.com/go-sql-driver/mysql
go get github.com/nictuku/dht

cd sn
go build
./sn
